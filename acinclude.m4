# VISIR_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([VISIR_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# VISIR_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([VISIR_SET_VERSION_INFO],
[
    visir_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    visir_major_version=`echo "$visir_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    visir_minor_version=`echo "$visir_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    visir_micro_version=`echo "$visir_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$visir_major_version"; then visir_major_version=0
    fi

    if test -z "$visir_minor_version"; then visir_minor_version=0
    fi

    if test -z "$visir_micro_version"; then visir_micro_version=0
    fi

    VISIR_VERSION="$visir_version"
    VISIR_MAJOR_VERSION=$visir_major_version
    VISIR_MINOR_VERSION=$visir_minor_version
    VISIR_MICRO_VERSION=$visir_micro_version

    if test -z "$4"; then VISIR_INTERFACE_AGE=0
    else VISIR_INTERFACE_AGE="$4"
    fi

    VISIR_BINARY_AGE=`expr 100 '*' $VISIR_MINOR_VERSION + $VISIR_MICRO_VERSION`
    VISIR_BINARY_VERSION=`expr 10000 '*' $VISIR_MAJOR_VERSION + \
                          $VISIR_BINARY_AGE`

    AC_SUBST(VISIR_VERSION)
    AC_SUBST(VISIR_MAJOR_VERSION)
    AC_SUBST(VISIR_MINOR_VERSION)
    AC_SUBST(VISIR_MICRO_VERSION)
    AC_SUBST(VISIR_INTERFACE_AGE)
    AC_SUBST(VISIR_BINARY_VERSION)
    AC_SUBST(VISIR_BINARY_AGE)

    AC_DEFINE_UNQUOTED(VISIR_MAJOR_VERSION, $VISIR_MAJOR_VERSION,
                       [VISIR major version number])
    AC_DEFINE_UNQUOTED(VISIR_MINOR_VERSION, $VISIR_MINOR_VERSION,
                       [VISIR minor version number])
    AC_DEFINE_UNQUOTED(VISIR_MICRO_VERSION, $VISIR_MICRO_VERSION,
                       [VISIR micro version number])
    AC_DEFINE_UNQUOTED(VISIR_INTERFACE_AGE, $VISIR_INTERFACE_AGE,
                       [VISIR interface age])
    AC_DEFINE_UNQUOTED(VISIR_BINARY_VERSION, $VISIR_BINARY_VERSION,
                       [VISIR binary version number])
    AC_DEFINE_UNQUOTED(VISIR_BINARY_AGE, $VISIR_BINARY_AGE,
                       [VISIR binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# VISIR_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([VISIR_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    if test -z "$apidocdir"; then
        apidocdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/html'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}'
    fi

    if test -z "$configdir"; then
        configdir='${datadir}/esopipes/${PACKAGE}-${VERSION}/config'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(apidocdir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)

    # evaluate $libdir -> $exec_prefix/lib -> full path, for maintainer mode
    eval private_libdir="$privatelibdir"
    eval private_libdir="$private_libdir"
    ac_configure_args="$ac_configure_args SWARPPREFIX=${private_libdir}"


    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(VISIR_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(VISIR_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

    # evaluate variables for maintainer mode
    eval config_dir="$configdir"
    eval config_dir="$config_dir"
    eval config_dir="$config_dir"
    AC_DEFINE_UNQUOTED(VISIR_CONFIG_PATH, "$config_dir",
                       [Absolute path to the config data directory])

    eval swarp_bin="${private_libdir}/bin/swarp"
    AC_DEFINE_UNQUOTED(VISIR_SWARP_BIN, "$swarp_bin",
                       [Absolute path to the swarp binary])

])


# VISIR_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([VISIR_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    VISIR_INCLUDES='-I$(top_srcdir)/visir -I$(top_srcdir)/irplib'
    VISIR_LDFLAGS='-L$(top_builddir)/visir'

    # No -L for IRPLIB which is statically linked
    all_includes='$(VISIR_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(VISIR_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    # Library aliases

    LIBVISIR='$(top_builddir)/visir/libvisir.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'

    # Substitute the defined symbols

    AC_SUBST(VISIR_INCLUDES)
    AC_SUBST(VISIR_LDFLAGS)

    AC_SUBST(LIBVISIR)
    AC_SUBST(LIBIRPLIB)

    # Check for CPL and user defined libraries
    AC_REQUIRE([ESO_CHECK_CPL])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(VISIR_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(VISIR_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])

# ESO_ENABLE_COVERAGE(coverage=no)
# enables coverage instrumentation option which sets up flags and defines
# ESO_COVERAGE_BUILD macro, default no
#-----------------------------
AC_DEFUN([ESO_ENABLE_COVERAGE],
[
    AC_ARG_ENABLE(coverage,
                  AS_HELP_STRING([--enable-coverage],
                                 [compiles with coverage instrumentation.
                                 [[default=$1]]]),
                  eso_enable_coverage=$enableval, eso_enable_coverage=$1)

    if test x"$eso_enable_coverage" = xyes; then
        ESO_PROG_CC_FLAG([-coverage], [CFLAGS="$CFLAGS --coverage"
                                     LDFLAGS="$LDFLAGS --coverage"])
        AC_DEFINE_UNQUOTED(ESO_COVERAGE_BUILD, 1, [Define to 1 iff build with coverage])
    fi
])

AC_DEFUN([ESO_GLIBC_MALLOC_CHECK],
[
# MALLOC_CHECK_ is not threadsafe, fixed in 2.11.1
# see glibc commit cc49a5a8837be1f9307b167d9bf4399798a847c9
    AC_MSG_CHECKING([glibc > 2.11.0])
    AC_LANG_PUSH(C)
    AC_RUN_IFELSE([AC_LANG_PROGRAM(
                      [[
    #include <stdio.h>
    #include <stdlib.h>
    #include <gnu/libc-version.h>
                      ]],
                      [
    int major = 0, minor = 0, patch = 0;
    if (sscanf(gnu_get_libc_version(), "%d.%d.%d", &major, &minor, &patch) < 2)
        exit(EXIT_FAILURE);
    if (major > 2 || (major == 2 && minor > 11) || (major == 2 && minor == 11 && patch >= 1))
        exit(EXIT_SUCCESS);
    else
        exit(EXIT_FAILURE);
                      ])],
                      [ESO_MALLOC_CHECK="2"; glibc_ok="yes"],
                      [ESO_MALLOC_CHECK="0"; glibc_ok="no"])

    AC_LANG_POP(C)
    AC_SUBST(ESO_MALLOC_CHECK)
    AC_MSG_RESULT([$glibc_ok])
])
