Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: visir
Upstream-Contact: ESO User Support Department <usd-help@eso.org>
Source: ftp://ftp.eso.org/pub/dfs/pipelines/visir/

Files: *
Copyright: Copyright (C) 2001-2012 European Southern Observatory
 2013 Ole Streicher (Debian packaging)
License: GPL-v2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: swarp/*
Copyright: 2010 IAP - CNRS / Universite P.&M.Curie
 1993-2014 Emmanuel Bertin - IAP/CNRS/UPMC,
 2005-2012 Chiara Marmo - IAP/CNRS,
License: GPL-3.0+
 SWarp is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 SWarp is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".
Comment: The files in src/wcs/ were originally released under GPL-2.0+ and
 LGPL-2.0+, but changed and relicensed under GPLv3+ by the upstream author.
