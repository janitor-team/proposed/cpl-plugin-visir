/* $Id: wkf_oca.h,v 1.2 2013-05-24 08:55:49 jtaylor Exp $
 *
 * This file is part of the ESO Common Pipeline Library
 * Copyright (C) 2013 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include "header.h"


if DPR.CATG like "%SCIENCE%" and DPR.TECH like "%IMAGE%" and DPR.TECH like "%CHOPNOD%" and DPR.TECH like "%JITTER%" and DPR.TYPE like "%OBJECT%" and INSTRUME == "VISIR" then
{
  DO.CATG = "IM_OBS_CHO_NOD_JIT";
  REFLEX.CATG = "IM_OBS_CHO_NOD_JIT";
  REFLEX.TARGET = IMAGE_REFLEX_TARGET;
}
if DPR.CATG like "%SCIENCE%" and DPR.TECH like "%IMAGE%" and DPR.TECH like "%CHOPNOD%" and DPR.TECH like "%JITTER%" and DPR.TECH like "%BURST%" and DPR.TYPE like "%OBJECT%" and INSTRUME == "VISIR" then
{
  DO.CATG = "IM_OBS_CHO_NOD_JIT_BURST";
  REFLEX.CATG = "IM_OBS_CHO_NOD_JIT_BURST";
  REFLEX.TARGET = IMAGE_REFLEX_TARGET;
}
if DPR.CATG == "SCIENCE" and DPR.TECH == "IMAGE,CHOPNOD" and DPR.TYPE == "OBJECT" and INSTRUME == "VISIR" then
{
  DO.CATG = "IM_OBS_CHO_NOD";
  REFLEX.CATG = "IM_OBS_CHO_NOD";
  REFLEX.TARGET = IMAGE_REFLEX_TARGET;
}
if DPR.CATG == "SCIENCE" and DPR.TECH == "IMAGE,BURST,CHOPNOD" and DPR.TYPE == "OBJECT" and INSTRUME == "VISIR" then
{
  DO.CATG = "IM_OBS_CHO_NOD_BURST";
  REFLEX.CATG = "IM_OBS_CHO_NOD_BURST";
  REFLEX.TARGET = IMAGE_REFLEX_TARGET;
}

if DPR.CATG like "%CALIB%" and DPR.TECH like "%IMAGE%" and DPR.TECH like "%CHOPNOD%" and DPR.TYPE like "%STD%" and INSTRUME == "VISIR" then
{
  DO.CATG = "IM_CAL_PHOT";
  REFLEX.CATG = "IM_CAL_PHOT";
  REFLEX.TARGET = "F";
}

if DPR.CATG like "%CALIB%" and DPR.TECH like "%IMAGE%" and DPR.TECH like "%CHOPNOD%" and DPR.TECH like "%BURST%" and DPR.TYPE like "%STD%" and INSTRUME == "VISIR" then
{
  DO.CATG = "IM_CAL_PHOT_BURST";
  REFLEX.CATG = "IM_CAL_PHOT_BURST";
  REFLEX.TARGET = "F";
}

if PRO.CATG like "%IMG_STD_CATALOG%" and INSTRUME == "VISIR" then
{
  DO.CATG = "IMG_STD_CATALOG";
  REFLEX.CATG = "IMG_STD_CATALOG";
}

if DPR.CATG like "%CALIB%" and DPR.TECH == "STATIC_MASK" and INSTRUME == "VISIR" then
{
  DO.CATG = "STATIC_MASK";
  REFLEX.CATG = "STATIC_MASK";
}
if DPR.CATG like "%CALIB%" and DPR.TECH like "%LINEARITY_TABLE%" and INSTRUME == "VISIR" then
{
  DO.CATG = "LINEARITY_TABLE";
  REFLEX.CATG = "LINEARITY_TABLE";
}

if DPR.CATG like "%SCIENCE%" and DPR.TECH like "%SPECTRUM%" and DPR.TECH like "%CHOPNOD%" and DPR.TYPE like "%OBJECT%" and INSTRUME == "VISIR" then
{
  DO.CATG = "SPEC_OBS_LMR";
  REFLEX.CATG = "SPEC_OBS_LMR";
  REFLEX.TARGET = SPECTRO_REFLEX_TARGET;
}

if DPR.CATG like "%CALIB%" and DPR.TECH like "%SPECTRUM%" and DPR.TECH like "%CHOPNOD%" and DPR.TYPE like "%STD%" and INSTRUME == "VISIR" then
{
  DO.CATG = "SPEC_CAL_PHOT";
  REFLEX.CATG = "SPEC_CAL_PHOT";
  REFLEX.TARGET = "F";
}

if DPR.CATG like "%SCIENCE%" and DPR.TECH like "%ECHELLE%" and DPR.TYPE like "%OBJECT%" and INSTRUME == "VISIR" then
{
  DO.CATG = "SPEC_OBS_HRG";
  REFLEX.CATG = "SPEC_OBS_HRG";
  REFLEX.TARGET = SPECTRO_REFLEX_TARGET;
}

if DPR.CATG like "%CALIB%" and DPR.TECH like "%ECHELLE%" and DPR.TYPE like "%STD%" and INSTRUME == "VISIR" then
{
  DO.CATG = "SPEC_CAL_PHOT_HRG";
  REFLEX.CATG = "SPEC_CAL_PHOT_HRG";
  REFLEX.TARGET = "F";
}

if PRO.CATG like "%SPEC_CAL_QEFF%" and INSTRUME == "VISIR" then
{
  DO.CATG = "SPEC_CAL_QEFF";
  REFLEX.CATG = "SPEC_CAL_QEFF";
}

if PRO.CATG like "%SPEC_CAL_LINES%" and INSTRUME == "VISIR" then
{
  DO.CATG = "SPEC_CAL_LINES";
  REFLEX.CATG = "SPEC_CAL_LINES";
}

if PRO.CATG like "%SPEC_STD_CATALOG%" and INSTRUME == "VISIR" then
{
  DO.CATG = "SPEC_STD_CATALOG";
  REFLEX.CATG = "SPEC_STD_CATALOG";
}

select execute(ACTION_PHOT_IMG) from inputFiles where DO.CATG == "IM_CAL_PHOT"
  group by INS.FILT1.NAME, INS.FILT2.NAME, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);
select execute(ACTION_PHOT_IMG_BURST) from inputFiles where DO.CATG == "IM_CAL_PHOT_BURST"
  group by INS.FILT1.NAME, INS.FILT2.NAME, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);
select execute(ACTION_COMBINE_IMG) from inputFiles where (DO.CATG == "IM_OBS_CHO_NOD" or DO.CATG == "IM_OBS_CHO_NOD_JIT")
  group by INS.FILT1.NAME, INS.FILT2.NAME, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);
select execute(ACTION_COMBINE_IMG_BURST) from inputFiles where (DO.CATG == "IM_OBS_CHO_NOD_BURST" or DO.CATG == "IM_OBS_CHO_NOD_JIT_BURST")
  group by INS.FILT1.NAME, INS.FILT2.NAME, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);

select execute(ACTION_PHOT_SPEC) from inputFiles where DO.CATG == "SPEC_CAL_PHOT"
  group by INS.GRAT1.WLEN, INS.GRAT2.WLEN, INS.SLIT1.NAME, INS.RESOL, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);
select execute(ACTION_COMBINE_SPEC) from inputFiles where DO.CATG == "SPEC_OBS_LMR"
  group by INS.GRAT1.WLEN, INS.GRAT2.WLEN, INS.SLIT1.NAME, INS.RESOL, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);
select execute(ACTION_PHOT_SPEC) from inputFiles where DO.CATG == "SPEC_CAL_PHOT_HRG"
  group by INS.GRAT1.WLEN, INS.GRAT2.WLEN, INS.SLIT1.NAME, INS.RESOL, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);
select execute(ACTION_COMBINE_SPEC) from inputFiles where DO.CATG == "SPEC_OBS_HRG"
  group by INS.GRAT1.WLEN, INS.GRAT2.WLEN, INS.SLIT1.NAME, INS.RESOL, INS.PFOV, OBS.ID, OBS.NAME, OBS.TARG.NAME, DET.SEQ1.DIT, DET.DIT, DET.READ.CURNAME, TPL.START as (TPL_A,tpl);

action ACTION_PHOT_IMG
{
minRet = 0; maxRet = 1;
select file as STATIC_MASK from calibFiles where DO.CATG == "STATIC_MASK";
minRet = 1; maxRet = 1;
select file as IMG_STD_CATALOG from calibFiles where DO.CATG == "IMG_STD_CATALOG";

recipe visir_img_phot;
product IMG_PHOT_COMBINED { REFLEX.CATG = "IMG_PHOT_COMBINED"; PRO.CATG = "IMG_PHOT_COMBINED"; PRO.EXT="tpl_0000.fits";}
}

action ACTION_COMBINE_IMG
{
minRet = 0; maxRet = 1;
select file as STATIC_MASK from calibFiles where DO.CATG == "STATIC_MASK";
minRet = 0; maxRet = 1;
select file as IMG_PHOT_COMBINED from calibFiles where PRO.CATG == "IMG_PHOT_COMBINED" and (inputFile.MJD-OBS - MJD-OBS) < 0.5 and (inputFile.MJD-OBS - MJD-OBS) > -0.5 and inputFile.INS.FILT1.NAME == INS.FILT1.NAME and inputFile.INS.PFOV == INS.PFOV;

recipe visir_img_combine;
product IMG_OBJ_COMBINED { PRO.CATG = "IMG_OBJ_COMBINED"; PRO.EXT="tpl_0001.fits";}
}

action ACTION_PHOT_IMG_BURST
{
minRet = 0; maxRet = 1;
select file as STATIC_MASK from calibFiles where DO.CATG == "STATIC_MASK";
minRet = 1; maxRet = 1;
select file as IMG_STD_CATALOG from calibFiles where DO.CATG == "IMG_STD_CATALOG";

recipe visir_img_phot;
product IMG_PHOT_COMBINED { REFLEX.CATG = "IMG_PHOT_COMBINED_BURST"; PRO.CATG = "IMG_PHOT_COMBINED_BURST"; PRO.EXT="tpl_0000.fits";}
}

action ACTION_COMBINE_IMG_BURST
{
minRet = 0; maxRet = 1;
select file as STATIC_MASK from calibFiles where DO.CATG == "STATIC_MASK";
minRet = 0; maxRet = 1;
select file as IMG_PHOT_COMBINED from calibFiles where PRO.CATG == "IMG_PHOT_COMBINED_BURST" and (inputFile.MJD-OBS - MJD-OBS) < 0.5 and (inputFile.MJD-OBS - MJD-OBS) > -0.5 and inputFile.INS.FILT1.NAME == INS.FILT1.NAME and inputFile.INS.PFOV == INS.PFOV;

recipe visir_img_combine;
product IMG_OBJ_COMBINED { PRO.CATG = "IMG_OBJ_COMBINED_BURST"; PRO.EXT="tpl_0001.fits";}
}


action ACTION_PHOT_SPEC
{
minRet = 0; maxRet = 1;
select file as STATIC_MASK from calibFiles where DO.CATG == "STATIC_MASK";
minRet = 1; maxRet = 1;
select file as SPEC_STD_CATALOG from calibFiles where DO.CATG == "SPEC_STD_CATALOG";
minRet = 1; maxRet = 1;
select file as SPEC_CAL_LINES from calibFiles where DO.CATG == "SPEC_CAL_LINES";
minRet = 1; maxRet = 1;
select file as SPEC_CAL_QEFF from calibFiles where DO.CATG == "SPEC_CAL_QEFF";
minRet = 0; maxRet = 1;
select file as LINEARITY_TABLE from calibFiles where DO.CATG == "LINEARITY_TABLE";

recipe visir_spc_phot;
product SPEC_PHOT_COMBINED { REFLEX.CATG = "SPEC_PHOT_COMBINED"; PRO.CATG = "SPEC_PHOT_COMBINED"; PRO.EXT="tpl_0000.fits";}
}

action ACTION_COMBINE_SPEC
{
minRet = 0; maxRet = 1;
select file as STATIC_MASK from calibFiles where DO.CATG == "STATIC_MASK";
minRet = 1; maxRet = 1;
select file as SPEC_CAL_LINES from calibFiles where DO.CATG == "SPEC_CAL_LINES";
minRet = 1; maxRet = 1;
select file as SPEC_CAL_QEFF from calibFiles where DO.CATG == "SPEC_CAL_QEFF";
minRet = 0; maxRet = 1;
select file as LINEARITY_TABLE from calibFiles where DO.CATG == "LINEARITY_TABLE";
minRet = 0; maxRet = 1;
select file as SPEC_PHOT_COMBINED from calibFiles where PRO.CATG == "SPEC_PHOT_COMBINED" and (inputFile.MJD-OBS - MJD-OBS) < 0.5 and (inputFile.MJD-OBS - MJD-OBS) > -0.5 and (inputFile.INS.GRAT1.WLEN == INS.GRAT1.WLEN or inputFile.INS.PRIS.WLEN == INS.PRIS.WLEN) and inputFile.INS.PFOV == INS.PFOV;

recipe visir_spc_obs;
product SPEC_OBJ_COMBINED { PRO.CATG = "SPEC_OBJ_COMBINED"; PRO.EXT="tpl_0001.fits";}
}
