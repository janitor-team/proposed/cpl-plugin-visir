/*
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003,2015,2016 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef VISIR_SPECTRO_H
#define VISIR_SPECTRO_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include "visir_spc_optmod.h"
#include "visir_inputs.h"
#include "irplib_framelist.h"
#include <cpl.h>

/* approximate pixel position of 8um and 13 um on aquarius */
#define VISIR_AQU_APPROX_WLEN8 140
#define VISIR_AQU_APPROX_WLEN13 840

typedef struct {
    const char * recipename;
    const cpl_parameterlist * parlist;
    cpl_boolean do_fixcombi;

    /* Inputs */
    int         plot;
    double      phi;
    double      ksi;
    double      eps;
    double      delta;

    /* only non zero for echelle */
    int         orderoffset;

    /* photometry */
    double      phot_emis_tol;
} visir_spc_config;

visir_spc_resol visir_spc_get_res_wl(const irplib_framelist *, double *,
                                     double *, double *, double *, int);

cpl_image * visir_spc_flip(const cpl_image *, double,
                           visir_spc_resol, visir_data_type);

cpl_bivector * visir_bivector_load_fits(const char *, const char *,
                                        const char *, int);
cpl_error_code visir_vector_resample(cpl_vector *, const cpl_vector *,
                                        const cpl_bivector *);
cpl_error_code visir_spc_extract_wcal(const cpl_image *, const cpl_image *,
                                      double, double, double, double,
                                      visir_spc_resol, int,
                                      const char*, const char *,
                                      cpl_table **,
                                      cpl_image **,
                                      cpl_propertylist *, int, int, int);

cpl_error_code visir_spc_wavecal(const cpl_image *,
                                 cpl_propertylist *,
                                 double, double, double,
                                 double, visir_spc_resol, int, const char *,
                                 const char *,
                                 cpl_table **, int, int);

cpl_error_code visir_spectro_qc(cpl_propertylist *, cpl_propertylist *,
                                cpl_boolean, const irplib_framelist *,
                                const char *, const char *);

cpl_error_code visir_spc_echelle_limit(int *, int *, double, int, int, int, int);
cpl_image * visir_spc_column_extract(const cpl_image *, int, int, int);


cpl_error_code
visir_spc_extract_order(cpl_image ** order,
                        cpl_image ** comorder,
                        const cpl_image * combined,
                        const cpl_image * imhcycle,
                        const double wlen,
                        const visir_spc_config * pconfig,
                        const cpl_boolean do_ech,
                        int);
#endif
