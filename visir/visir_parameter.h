/* $Id: visir_parameter.h,v 1.12 2012-05-07 08:28:06 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2012-05-07 08:28:06 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_PARAMETER_H
#define VISIR_PARAMETER_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "visir_utils.h"

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/

/* The available recipe parameters */
/* The value 1 is unused, which makes the (mis)use of logical or detectable */
#define VISIR_PARAM_NODPOS   ((visir_parameter) 1 <<  1)
#define VISIR_PARAM_AUTOBPM  ((visir_parameter) 1 <<  2)
#define VISIR_PARAM_GLITCH   ((visir_parameter) 1 <<  3)
#define VISIR_PARAM_PURGE    ((visir_parameter) 1 <<  4)
#define VISIR_PARAM_UNION    ((visir_parameter) 1 <<  5)
#define VISIR_PARAM_REJECT   ((visir_parameter) 1 <<  6)
#define VISIR_PARAM_PLOT     ((visir_parameter) 1 <<  7)
#define VISIR_PARAM_SLITSKEW ((visir_parameter) 1 <<  8)
#define VISIR_PARAM_SPECSKEW ((visir_parameter) 1 <<  9)
#define VISIR_PARAM_VERTARC  ((visir_parameter) 1 << 10)
#define VISIR_PARAM_HORIARC  ((visir_parameter) 1 << 11)
#define VISIR_PARAM_ORDEROFF ((visir_parameter) 1 << 12)
#define VISIR_PARAM_OFFSETS  ((visir_parameter) 1 << 13)
#define VISIR_PARAM_OBJECTS  ((visir_parameter) 1 << 14)
#define VISIR_PARAM_REFINE   ((visir_parameter) 1 << 15)
#define VISIR_PARAM_XCORR    ((visir_parameter) 1 << 16)
#define VISIR_PARAM_JYVAL    ((visir_parameter) 1 << 17)
#define VISIR_PARAM_RADII    ((visir_parameter) 1 << 18)
#define VISIR_PARAM_LOWLIM   ((visir_parameter) 1 << 19)
#define VISIR_PARAM_HIGHLIM  ((visir_parameter) 1 << 20)
#define VISIR_PARAM_FIXCOMBI ((visir_parameter) 1 << 21)
#define VISIR_PARAM_EMIS_TOL ((visir_parameter) 1 << 22)
#define VISIR_PARAM_ZERODIST ((visir_parameter) 1 << 23)
#define VISIR_PARAM_QEFF     ((visir_parameter) 1 << 24)
#define VISIR_PARAM_REJBORD  ((visir_parameter) 1 << 25)
#define VISIR_PARAM_HOT_LIM  ((visir_parameter) 1 << 26)
#define VISIR_PARAM_COLD_LIM ((visir_parameter) 1 << 27)
#define VISIR_PARAM_DEV_LIM  ((visir_parameter) 1 << 28)
#define VISIR_PARAM_NSAMPLES ((visir_parameter) 1 << 29)
#define VISIR_PARAM_HALFSIZE ((visir_parameter) 1 << 30)
#define VISIR_PARAM_COMBINE  ((visir_parameter) 1 << 31)
#define VISIR_PARAM_STRIPNON ((visir_parameter) 1 << 32)
#define VISIR_PARAM_STRIPITE ((visir_parameter) 1 << 33)
#define VISIR_PARAM_STRIPMOR ((visir_parameter) 1 << 34)
#define VISIR_PARAM_REJLEFT  ((visir_parameter) 1 << 35)
#define VISIR_PARAM_REJRIGHT ((visir_parameter) 1 << 36)
#define VISIR_PARAM_ECCMAX   ((visir_parameter) 1 << 37)
#define VISIR_PARAM_BKG_CORRECT ((visir_parameter) 1 << 38)


/*-----------------------------------------------------------------------------
                                       Prototypes
 -----------------------------------------------------------------------------*/

typedef unsigned long long visir_parameter;

cpl_error_code visir_parameter_set(cpl_parameterlist *, const char *,
                                   visir_parameter);

cpl_boolean visir_parameterlist_get_bool(const cpl_parameterlist *,
                                         const char *,
                                         visir_parameter);

int visir_parameterlist_get_int(const cpl_parameterlist *,
                                const char *,
                                visir_parameter);

double visir_parameterlist_get_double(const cpl_parameterlist *,
                                      const char *,
                                      visir_parameter);

const char * visir_parameterlist_get_string(const cpl_parameterlist *,
                                            const char *,
                                            visir_parameter);
#endif
