/* $Id: visir_spc_distortion.h,v 1.16 2012-05-30 08:16:24 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2012-05-30 08:16:24 $
 * $Revision: 1.16 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_SPC_DISTORTION_H
#define VISIR_SPC_DISTORTION_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "visir_inputs.h"
#include "visir_spc_optmod.h"

/*-----------------------------------------------------------------------------
                                       Prototypes
 -----------------------------------------------------------------------------*/

#define VISIR_DRS_DIST_KSI (0.7 * CPL_MATH_RAD_DEG)
#define VISIR_DRS_DIST_EPS 1.04
#define VISIR_DRS_DIST_DELTA 0.08
#define VISIR_DRS_DIST_PHI (1.6 * CPL_MATH_RAD_DEG)

cpl_error_code visir_spc_det_fix(cpl_image **, int, cpl_boolean,
                                 double, visir_spc_resol, double, double,
                                 double, double, int);

cpl_error_code visir_spc_det_warp(cpl_image **, int, double, double,
                                  double, double, double, double);

#endif
