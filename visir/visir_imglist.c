/* $Id: visir_imglist.c,v 1.8 2013-03-22 16:20:04 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2012 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-03-22 16:20:04 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "visir_utils.h"

#include <string.h>
#include <assert.h>

/*-----------------------------------------------------------------------------
                                   Define
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * @defgroup visir_imglist list containing images and auxiliary data
 *
 * TBD
 */
/*----------------------------------------------------------------------------*/


visir_imglist *
visir_imglist_new(const cpl_size capacity, cpl_propertylist * mplist)
{
    visir_imglist * l = cpl_calloc(1, sizeof(visir_imglist));
    l->_capacity = capacity <= 0 ? 1 : capacity;
    l->_size = 0;
    l->auxdata = cpl_calloc(l->_capacity, sizeof(void*));;
    l->mplist = mplist;
    l->imgs = cpl_imagelist_new();
    return l;
}

void
visir_imglist_delete(visir_imglist * l, visir_free aux_destructor)
{
    if (l != NULL) {
        cpl_imagelist_delete(l->imgs);
        if (aux_destructor)
            for (cpl_size i = 0; i < visir_imglist_get_size(l); i++) {
                void * data = visir_imglist_get_data(l, i);
                if (data)
                    aux_destructor(data);
            }
        cpl_free(l->auxdata);

        cpl_free(l);
    }
}

void
visir_imglist_unwrap(visir_imglist * l, visir_free aux_destructor)
{
    if (l != NULL) {
        visir_imagelist_unwrap(l->imgs);
        if (aux_destructor)
            for (cpl_size i = 0; i < visir_imglist_get_size(l); i++) {
                void * data = visir_imglist_get_data(l, i);
                if (data)
                    aux_destructor(data);
            }
        cpl_free(l->auxdata);
        cpl_free(l);
    }
}

static void
visir_imglist_resize(visir_imglist *imgs, const cpl_size n)
{
    if (n < imgs->_capacity)
        return;
    imgs->auxdata = cpl_realloc(imgs->auxdata, n * sizeof(void *));
    imgs->_capacity = n;
}

void
visir_imglist_append(visir_imglist * l, cpl_image * img,
                     void *auxdata)
{
    if (l->_size == l->_capacity)
        visir_imglist_resize(l, l->_capacity * 2);
    cpl_imagelist_set(l->imgs, img, cpl_imagelist_get_size(l->imgs));
    l->auxdata[l->_size] = auxdata;
    l->_size++;
    assert(l->_size == cpl_imagelist_get_size(l->imgs));
}

void
visir_imglist_append_imglist(visir_imglist * l, cpl_imagelist * imgs,
                             void ** auxdata)
{
    for (cpl_size i = 0; i < cpl_imagelist_get_size(imgs); i++) {
        void * data = auxdata == NULL ? NULL : auxdata[i];
        cpl_image * img = cpl_imagelist_get(imgs, i);
        visir_imglist_append(l, cpl_image_duplicate(img), data);
    }
}

cpl_size
visir_imglist_get_size(const visir_imglist * imgs)
{
    return imgs->_size;
}

cpl_image *
visir_imglist_get_img(const visir_imglist * imgs, const cpl_size index)
{
    cpl_ensure(index < imgs->_size, CPL_ERROR_ACCESS_OUT_OF_RANGE, NULL);
    cpl_ensure(index >= 0, CPL_ERROR_ACCESS_OUT_OF_RANGE, NULL);
    return cpl_imagelist_get(imgs->imgs, index);
}

cpl_error_code
visir_imglist_set_data(visir_imglist * imgs,
                       const cpl_size index, void * auxdata)
{
    cpl_ensure_code(index < imgs->_size, CPL_ERROR_ACCESS_OUT_OF_RANGE);
    cpl_ensure_code(index >= 0, CPL_ERROR_ACCESS_OUT_OF_RANGE);
    imgs->auxdata[index] = auxdata;
    return CPL_ERROR_NONE;
}

cpl_error_code
visir_imglist_get(const visir_imglist * imgs, const cpl_size index,
                  cpl_image ** img, void ** auxdata)
{
    cpl_ensure_code(index < imgs->_size, CPL_ERROR_ACCESS_OUT_OF_RANGE);
    cpl_ensure_code(index >= 0, CPL_ERROR_ACCESS_OUT_OF_RANGE);
    if (img)
        *img = visir_imglist_get_img(imgs, index);
    if (auxdata)
        *auxdata = visir_imglist_get_data(imgs, index);
    return CPL_ERROR_NONE;
}

void *
visir_imglist_get_data(const visir_imglist * imgs, const cpl_size index)
{
    cpl_ensure(index < imgs->_size, CPL_ERROR_ACCESS_OUT_OF_RANGE, NULL);
    cpl_ensure(index >= 0, CPL_ERROR_ACCESS_OUT_OF_RANGE, NULL);
    return imgs->auxdata[index];
}

void
visir_imglist_set_mplist(visir_imglist * imgs, cpl_propertylist * mplist)
{
    imgs->mplist = mplist;
}

cpl_propertylist *
visir_imglist_get_mplist(const visir_imglist * imgs)
{
    return imgs->mplist;
}

cpl_imagelist *
visir_imglist_get_imglist(const visir_imglist * imgs)
{
    return imgs->imgs;
}
