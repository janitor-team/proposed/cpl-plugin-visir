/* $Id: visir_inputs.h,v 1.55 2013-02-21 16:39:11 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-02-21 16:39:11 $
 * $Revision: 1.55 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_INPUTS_H
#define VISIR_INPUTS_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "irplib_framelist.h"

#include "visir_spc_optmod.h"

/*-----------------------------------------------------------------------------
                                   Define
 -----------------------------------------------------------------------------*/

/* The values range from 0 (or 1) to 2**16-1 (or 2**16) */
#define VISIR_HCYCLE_BPM_THRESHOLD  65000.0

/* Due to a inconsistency in the chosen VISIR FITS-format the pixels in
   the half-cycle image are offset, such that the lowest pixel value is
   -32768 (or perhaps -32767?). This is fixed by adding 32768 to each
   pixel value */
#define VISIR_HCYCLE_OFFSET  32768.0

typedef enum {
    /* VISIR nod/chopping mode detection */

    /* Mode can be assumed to be: PERPENDICULAR */
    VISIR_CHOPNOD_PERPENDICULAR,

    /* Mode can be assumed to be: PARALLEL */
    VISIR_CHOPNOD_PARALLEL,
    
    /* Mode is determined from FITS header */
    VISIR_CHOPNOD_AUTO

} visir_chopnod_mode;

typedef enum {
    VISIR_ERROR,
    VISIR_VARIANCE,
    VISIR_WEIGHT
} visir_error_type;

typedef enum {
    VISIR_DATA_CUBE1,
    VISIR_DATA_CUBE2,
    VISIR_DATA_BURST,
    VISIR_DATA_AQU_HCYCLE,
    VISIR_DATA_AQU_BURST,
    VISIR_DATA_AQU_BURST_EXT,
    /* only INT frame, no half cycles, format of an 2015 commissioning, can
     * probably be removed in future */
    VISIR_DATA_AQU_INT,
} visir_data_type;

static inline cpl_boolean
visir_data_is_aqu(const visir_data_type dtype)
{
    return dtype == VISIR_DATA_AQU_HCYCLE || dtype == VISIR_DATA_AQU_BURST ||
        dtype == VISIR_DATA_AQU_BURST_EXT || dtype == VISIR_DATA_AQU_INT;
}

static inline cpl_boolean
visir_data_is_drs(const visir_data_type dtype)
{
    return !visir_data_is_aqu(dtype);
}

static inline cpl_boolean
visir_data_is_burst(const visir_data_type dtype)
{
    return dtype == VISIR_DATA_BURST || dtype == VISIR_DATA_AQU_BURST ||
        dtype == VISIR_DATA_AQU_BURST_EXT;
}


/*-----------------------------------------------------------------------------
                                   Functions prototypes
 -----------------------------------------------------------------------------*/

cpl_imagelist * visir_inputs_combine(const char *, const cpl_parameterlist *,
                                     const irplib_framelist *, const char *,
                                     const char *,  int *, cpl_boolean,
                                     double, visir_spc_resol);
cpl_image ** visir_img_recombine_list(const char *,
                                      const cpl_parameterlist *,
                                      cpl_imagelist *,
                                      const cpl_propertylist **,
                                      cpl_geom_combine, cpl_boolean *);
cpl_image ** visir_img_recombine(const char *, const cpl_parameterlist *,
                                 const irplib_framelist *, const char *,
                                 const char *, cpl_geom_combine, cpl_boolean *,
                                 cpl_boolean, double, visir_spc_resol);
cpl_imagelist * visir_load_imagelist(const irplib_framelist *, int,
                                     cpl_boolean);
cpl_imagelist * visir_load_hcycle(const irplib_framelist *, int);
cpl_error_code visir_image_reject_hot(cpl_image *, const char *);

cpl_imagelist * visir_imagelist_load_last(const irplib_framelist *);

cpl_image ** visir_img_collapse_beam(cpl_propertylist        *,
                                     const cpl_image         *,
                                     const cpl_parameterlist *,
                                     const char              *,
                                     visir_chopnod_mode,
                                     const cpl_propertylist *);

double visir_img_check_box(const cpl_apertures *, int, int,
                           const cpl_apertures *, int, int,
                           double, double, cpl_boolean *, cpl_boolean *);

double visir_img_check_align(const cpl_apertures *, int,
                             const cpl_apertures *, int, int,
                             double, double,
                             cpl_boolean *);

double visir_img_check_line(const cpl_apertures *, int,
                            const cpl_apertures *, int,
                            double, double);

cpl_error_code visir_get_data_type(const cpl_frame * frame,
                                   const cpl_propertylist * plist,
                                   visir_data_type * ptype, cpl_size * pnext);

cpl_bivector * visir_load_lintable(cpl_frame * linframe, cpl_boolean is_spec);

cpl_image *
visir_load_bpm(cpl_frame * frm, visir_data_type dtype, cpl_boolean is_spec);

cpl_error_code visir_load_cube2_split(cpl_imagelist *, cpl_imagelist *,
                                      const irplib_framelist *, const int,
                                      const int, const int);

cpl_error_code visir_load_burst(cpl_imagelist *, cpl_imagelist *,
                                const cpl_frame *, const cpl_propertylist *,
                                const int, const int, const int, const int,
                                const int, const int);
cpl_error_code visir_load_burst_aqu(cpl_imagelist *, cpl_imagelist *,
                                    const cpl_frame *, const cpl_propertylist *,
                                    const int, const int, const int);

cpl_error_code visir_img_burst_find_delta_chop(const cpl_propertylist *,
                                               int *, int *);


visir_chopnod_mode visir_img_find_beam(cpl_propertylist        *,
                                       const cpl_image         *,
                                       const cpl_image         *,
                                       const cpl_propertylist  *,
                                       const cpl_parameterlist *,
                                       const char              *,
                                       double                  [],
                                       double                  []);

cpl_error_code visir_img_find_beam_two(cpl_propertylist *, const cpl_image  *,
                                       const cpl_image  *, double, double, double,
                                       double [], double []);

#endif
