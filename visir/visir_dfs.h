/* $Id: visir_dfs.h,v 1.96 2013-05-14 12:59:23 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-05-14 12:59:23 $
 * $Revision: 1.96 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_DFS_H
#define VISIR_DFS_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/

/* Define here the PRO.CATG keywords */

#define VISIR_IMG_COMBINE_COMBINED_PROCATG_CNJ "IMG_OBS_COMBINED_CNJ"
#define VISIR_IMG_COMBINE_COMBINED_PROCATG_CJ "IMG_OBS_COMBINED_CJ"
#define VISIR_IMG_COMBINE_COMBINED_PROCATG_NJ "IMG_OBS_COMBINED_NJ"
#define VISIR_IMG_COMBINE_COMBINED_PROCATG_DJ "IMG_OBS_COMBINED_DJ"

#define VISIR_IMG_COMBINE_ONEBEAM_PROCATG_CNJ "IMG_OBS_ONEBEAM_CNJ"
#define VISIR_IMG_COMBINE_ONEBEAM_PROCATG_CJ  "IMG_OBS_ONEBEAM_CJ"
#define VISIR_IMG_COMBINE_ONEBEAM_PROCATG_NJ  "IMG_OBS_ONEBEAM_NJ"
#define VISIR_IMG_COMBINE_ONEBEAM_PROCATG_DJ  "IMG_OBS_ONEBEAM_DJ"

#define VISIR_IMG_PFOV_TAB_PROCATG_BIN      "IMG_PFOV_TAB_BIN"
#define VISIR_IMG_PFOV_TAB_PROCATG_TEL      "IMG_PFOV_TAB_TEL"
#define VISIR_IMG_PSF_TAB_PROCATG           "IMG_PSF_TAB"
#define VISIR_IMG_FOCFWHM_TAB_PROCATG       "IMG_FOCUS_TAB"
#define VISIR_IMG_ILLU_FITTED_PROCATG       "IMG_ILLU_MAP"
#define VISIR_IMG_ILLU_TAB_PROCATG          "IMG_ILLU_TAB"
#define VISIR_IMG_TRANS_TAB_PROCATG         "IMG_TRANS_TAB"
#define VISIR_IMG_ACHRO_TAB_PROCATG         "IMG_ACHRO_TAB"
#define VISIR_IMG_PHOT_COMBINED_PROCATG     "IMG_PHOT_COMBINED"
#define VISIR_IMG_PHOT_ONEBEAM_PROCATG      "IMG_PHOT_ONEBEAM"
#define VISIR_IMG_OBJ_COMBINED_PROCATG      "IMG_OBJ_COMBINED"
#define VISIR_IMG_OBJ_ONEBEAM_PROCATG       "IMG_OBJ_ONEBEAM"
#define VISIR_IMG_OBJ_CALIBRATED_PROCATG    "IMG_OBJ_CALIBRATED"
#define VISIR_IMG_FF_PROCATG                "IMG_FF"
#define VISIR_IMG_FF_BPM_PROCATG            "IMG_BPM"
#define VISIR_IMG_TECH_FF_PROCATG           "IMG_FF_TECH"
#define VISIR_IMG_TECH_FF_BPM_PROCATG       "IMG_BPM_TECH"
#define VISIR_IMG_DARK_AVG_PROCATG          "IMG_DARK_AVG"
#define VISIR_IMG_DARK_HOT_PROCATG          "IMG_DARK_HOT"
#define VISIR_IMG_DARK_COLD_PROCATG         "IMG_DARK_COLD"
#define VISIR_IMG_DARK_DEV_PROCATG          "IMG_DARK_DEV"

#define VISIR_SPC_OBS_TAB_PROCATG           "SPC_OBS_LMR_TAB"
#define VISIR_SPC_OBS_COMBINED_PROCATG      "SPC_OBS_LMR_COMBINED"
#define VISIR_SPC_OBS_WEIGHT_PROCATG        "SPC_OBS_LMR_WEIGHT"
#define VISIR_SPC_PHOT_COMBINED_PROCATG     "SPC_PHOT_COMBINED"
#define VISIR_SPC_PHOT_TAB_PROCATG          "SPC_PHOT_TAB"
#define VISIR_SPC_PHOT_WEIGHT_PROCATG       "SPC_PHOT_WEIGHT"
#define VISIR_SPC_PHOT_ECH_COMBINED_PROCATG "SPC_PHOT_HRG_COMBINED"
#define VISIR_SPC_PHOT_ECH_TAB_PROCATG      "SPC_PHOT_HRG_TAB"
#define VISIR_SPC_PHOT_ECH_WEIGHT_PROCATG   "SPC_PHOT_HRG_WEIGHT"
#define VISIR_SPC_WCAL_TAB_PROCATG          "SPC_WCAL_LMR_TAB"
#define VISIR_SPC_OBS_ECH_TAB_PROCATG       "SPC_OBS_HRG_TAB"
#define VISIR_SPC_OBS_ECH_COMBINED_PROCATG  "SPC_OBS_HRG_COMBINED"
#define VISIR_SPC_OBS_ECH_WEIGHT_PROCATG    "SPC_OBS_HRG_WEIGHT"
#define VISIR_SPC_WCAL_ECH_TAB_PROCATG      "SPC_WCAL_HRG_TAB"
#define VISIR_SPC_FF_PROCATG                "SPEC_FF"
#define VISIR_SPC_FF_BPM_PROCATG            "SPEC_BPM"
#define VISIR_SPC_TECH_FF_PROCATG           "SPEC_FF_TECH"
#define VISIR_SPC_TECH_FF_BPM_PROCATG       "SPEC_BPM_TECH"
#define VISIR_SPC_DARK_AVG_PROCATG          "SPEC_DARK_AVG"
#define VISIR_SPC_DARK_HOT_PROCATG          "SPEC_DARK_HOT"
#define VISIR_SPC_DARK_COLD_PROCATG         "SPEC_DARK_COLD"
#define VISIR_SPC_DARK_DEV_PROCATG          "SPEC_DARK_DEV"

#define VISIR_IMG_COADDED_IMG                "COADDED_IMAGE_COMBINED"
#define VISIR_IMG_COADDED_WGT                "COADDED_WEIGHT_COMBINED"
#define VISIR_IMG_COADDED_CTR                "COADDED_CONTRIBUTION_COMBINED"

#define VISIR_UTIL_INPUTS_COMBINED_PROCATG   "IMG_COMBINED"
#define VISIR_UTIL_REPACK_A_ON_PROCATG       "IMG_REPACKED_A_ON"
#define VISIR_UTIL_REPACK_A_OFF_PROCATG      "IMG_REPACKED_A_OFF"
#define VISIR_UTIL_REPACK_MEAN_PROCATG       "IMG_REPACKED_MEAN"
#define VISIR_UTIL_REPACK_MEAN_A_PROCATG     "IMG_REPACKED_MEAN_A"
#define VISIR_UTIL_REPACK_MEAN_A_ON_PROCATG  "IMG_REPACKED_MEAN_A_ON"
#define VISIR_UTIL_REPACK_MEAN_A_OFF_PROCATG "IMG_REPACKED_MEAN_A_OFF"
#define VISIR_UTIL_REPACK_B_ON_PROCATG       "IMG_REPACKED_B_ON"
#define VISIR_UTIL_REPACK_B_OFF_PROCATG      "IMG_REPACKED_B_OFF"
#define VISIR_UTIL_REPACK_MEAN_B_PROCATG     "IMG_REPACKED_MEAN_B"
#define VISIR_UTIL_REPACK_MEAN_B_ON_PROCATG  "IMG_REPACKED_MEAN_B_ON"
#define VISIR_UTIL_REPACK_MEAN_B_OFF_PROCATG "IMG_REPACKED_MEAN_B_OFF"
#define VISIR_UTIL_DETECT_SHIFT_PROCATG      "BEAM_DETECTED"
#define VISIR_UTIL_DETECT_SHIFT_TEMPLATE_PROCATG "CORRELATION_TEMPLATE"
#define VISIR_UTIL_CONVERT_PROCATG          "IMG_CONVERTED"
#define VISIR_UTIL_CONVERT_AUX_PROCATG      "AUX_CONVERTED"
#define VISIR_UTIL_UNDISTORT_PROCATG        "UNDISTORTED"
#define VISIR_IMA_STD_CAT_PROCATG           "IMG_STD_CATALOG"
#define VISIR_SPEC_STD_CAT_PROCATG          "SPEC_STD_CATALOG"
#define VISIR_SPEC_CAL_LINES_PROCATG        "SPEC_CAL_LINES"
#define VISIR_SPEC_CAL_QEFF_PROCATG         "SPEC_CAL_QEFF"

#define VISIR_IMG_CLIPPED_PROCATG           "BPM"
#define VISIR_IMG_CLIPPED_MAP_PROCATG       "CONTRIBUTION_MAP"

#define VISIR_UTIL_WEIGHT2ERROR_PROCATG     "ERROR_MAP"
#define VISIR_UTIL_ERROR_MAP_PROCATG        "ERROR_MAP"
#define VISIR_UTIL_WEIGHT_MAP_PROCATG       "WEIGHT_MAP"
#define VISIR_UTIL_VARIANCE_MAP_PROCATG     "VARIANCE_MAP"
#define VISIR_UTIL_NODDED                   "NODDED"
#define VISIR_UTIL_NODDED_REFINED_POS       "NODDED_REFINED"

#define VISIR_UTIL_JOIN_PROCATG             "JOIN"
#define VISIR_UTIL_QC_PROCATG            "QC_HEADER"

/* Define here the DO.CATG keywords */

#define VISIR_UTIL_DATA                     "DATA"
#define VISIR_CALIB_FLAT                    "FLAT"
#define VISIR_CALIB_BPM                     "BPM"
#define VISIR_CALIB_STATIC_MASK             "STATIC_MASK"
#define VISIR_CALIB_STDSTAR_IMG             "IMG_STD_CATALOG"             
#define VISIR_CALIB_STDSTAR_SPC             "SPEC_STD_CATALOG"             
#define VISIR_CALIB_LINES_SPC               "SPEC_CAL_LINES"
#define VISIR_CALIB_QEFF_SPC                "SPEC_CAL_QEFF"
#define VISIR_CALIB_LIN                     "LINEARITY_TABLE"

#define VISIR_EXTN_ERROR                    "ERROR MAP"
#define VISIR_EXTN_WEIGHT                   "WEIGHT MAP"
#define VISIR_EXTN_BPM                      "BAD PIXEL MAP"

#define VISIR_ACQ_CNJ                       "ACQ_OBS_CHO_NOD_JIT"
#define VISIR_IMG_BURST                     "BURST"
#define VISIR_IMG_ACHRO_RAW                 "IM_CAL_ACHRO"
#define VISIR_IMG_COMBINE_CN                "IM_OBS_CHO_NOD"
#define VISIR_IMG_COMBINE_CN_BURST          "IM_OBS_CHO_NOD_BURST"
#define VISIR_IMG_COMBINE_CNJ               "IM_OBS_CHO_NOD_JIT"
#define VISIR_IMG_COMBINE_CNJ_BURST         "IM_OBS_CHO_NOD_JIT_BURST"
#define VISIR_IMG_COMBINE_CJ                "IM_OBS_CHO_JIT"
#define VISIR_IMG_COMBINE_NJ                "IM_OBS_NOD_JIT"
#define VISIR_IMG_COMBINE_DJ                "IM_OBS_DIR_JIT"
#define VISIR_IMG_CAL_PHOT                  "IM_CAL_PHOT"
#define VISIR_IMG_CAL_PHOT_PP               "IM_CAL_PHOT_PREPROCESSED"
#define VISIR_IMG_CAL_PHOT_BURST            "IM_CAL_PHOT_BURST"
#define VISIR_IMG_CAL_OBJ                   "IM_CAL_OBJECT"
#define VISIR_IMG_CAL_OBJ_BURST             "IM_CAL_OBJECT_BURST"
#define VISIR_IMG_FF_RAW                    "IM_CAL_FLAT"
#define VISIR_IMG_TECH_FF_RAW               "IM_TECH_FLAT"
#define VISIR_IMG_FOCFWHM_RAW               "IM_TEC_FOCUS"
#define VISIR_IMG_ILLU_RAW                  "IM_CAL_ILLU"
#define VISIR_IMG_PFOV_BIN                  "IM_CAL_PFOV_BIN"
#define VISIR_IMG_PFOV_TEL                  "IM_CAL_PFOV_TEL"
#define VISIR_IMG_PHOT_RAW                  "IM_CAL_PHOT"
#define VISIR_IMG_PSF_RAW                   "IM_CAL_PSF"
#define VISIR_IMG_TRANS_RAW                 "IM_TEC_TRANS"
#define VISIR_IMG_DARK_RAW                  "IM_CAL_DARK"
#define VISIR_SPC_OBS_ECH_RAW               "SPEC_OBS_HRG"
#define VISIR_SPC_OBS_RAW                   "SPEC_OBS_LMR"
#define VISIR_SPC_OBS_PP                    "SPEC_OBS_LMR_PREPROCESSED"
#define VISIR_SPC_OBS_SKYFRAME              "SPEC_OBS_LMR_SKYFRAME"
#define VISIR_SPC_OBS_ECH_PP                "SPEC_OBS_HRG_PREPROCESSED"
#define VISIR_SPC_OBS_HR_RAW                "SPEC_OBS_HR"
#define VISIR_SPC_PHOT_RAW                  "SPEC_CAL_PHOT"
#define VISIR_SPC_PHOT_PP                   "SPEC_CAL_PHOT_PREPROCESSED"
#define VISIR_SPC_PHOT_ECH_RAW              "SPEC_CAL_PHOT_HRG"
#define VISIR_SPC_PHOT_ECH_PP               "SPEC_CAL_PHOT_HRG_PREPROCESSED"
#define VISIR_SPC_WCAL_ECH_RAW              "SPEC_CAL_HRG_WCAL"
#define VISIR_SPC_WCAL_RAW                  "SPEC_CAL_LMR_WCAL"
#define VISIR_SPC_FF_RAW                    "SPEC_CAL_FLAT"
#define VISIR_SPC_TECH_FF_RAW               "SPEC_TECH_FLAT"
#define VISIR_SPC_DARK_RAW                  "SPEC_CAL_DARK"

#define VISIR_SAM_CAL_N_RAW                 "SAM_CAL_N"
#define VISIR_SAM_CAL_CN_RAW                "SAM_CAL_CN"
#define VISIR_SAM_CAL_NJ_RAW                "SAM_CAL_NJ"
#define VISIR_SAM_CAL_CNJ_RAW               "SAM_CAL_CNJ"
#define VISIR_CORO_CAL_CNJ_RAW              "CORO_CAL_CNJ"
#define VISIR_SAM_OBS_N_RAW                 "SAM_OBS_N"
#define VISIR_SAM_OBS_CN_RAW                "SAM_OBS_CN"
#define VISIR_SAM_OBS_NJ_RAW                "SAM_OBS_NJ"
#define VISIR_SAM_OBS_CNJ_RAW               "SAM_OBS_CNJ"
#define VISIR_CORO_OBS_CNJ_RAW              "CORO_OBS_CNJ"

#define VISIR_UTIL_WEIGHT2ERROR             "WEIGHT_MAP"
#define VISIR_UTIL_WEIGHT_MAP               "WEIGHT_MAP"
#define VISIR_UTIL_ERROR_MAP                "ERROR_MAP"
#define VISIR_UTIL_VARIANCE_MAP             "VARIANCE_MAP"
#define VISIR_UTIL_INPUTS_RAW               "RAW"
#define VISIR_UTIL_CORRECTED                "BKG_CORRECTED"
#define VISIR_UTIL_REPACK_RAW               "CUBE2"
#define VISIR_UTIL_CONVERT_RAW              "POS_CONVERT"
#define VISIR_UTIL_CONVERT_TAB              "POS_TABLE"
#define VISIR_UTIL_CONVERT_INV              "INV_CONVERT"
#define VISIR_UTIL_CONVERT_POS_AUX          "POS_AUX"
#define VISIR_UTIL_CONVERT_INV_AUX          "INV_AUX"
#define VISIR_UTIL_UNDISTORT_RAW            "CHOPNOD_CORRECTED"
#define VISIR_IMG_LINES_ASCII               "IMG_LINES_ASCII"
#define VISIR_SPC_LINES_ASCII               "SPC_LINES_ASCII"
#define VISIR_SPC_QEFF_ASCII                "SPC_QEFF_ASCII"
#define VISIR_SPC_CAT_ASCII                 "SPC_CAT_ASCII"

#define MAN_VISIR_CALIB_BPM_IMG "\nAdditionally, a bad pixel map with a " \
"PRO.CATG of " VISIR_IMG_FF_BPM_PROCATG "\nmay be added to the Set Of Frames " \
"with tag: " VISIR_CALIB_BPM "."

#define MAN_VISIR_CALIB_BPM_SPC "\nAdditionally, a bad pixel map with a " \
"PRO.CATG of " VISIR_SPC_FF_BPM_PROCATG "\nmay be added to the Set Of Frames " \
"with tag: " VISIR_CALIB_BPM "."


/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

int visir_dfs_set_groups(cpl_frameset *);
const char *
visir_dfs_output_catg(const char *, const char *);
cpl_error_code visir_dfs_check_framelist_tag(const irplib_framelist *);

#endif
