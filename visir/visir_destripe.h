/* $Id: visir_destripe.h,v 1.6 2012-02-09 13:44:15 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2012-02-09 13:44:15 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_DESTRIPE_H
#define VISIR_DESTRIPE_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>


#ifndef VISIR_DESTRIPE_DETECT
#define VISIR_DESTRIPE_DETECT           (3.5 * 1.64)
#endif

#ifndef VISIR_DESTRIPE_DETECT_THRESHOLD
#define VISIR_DESTRIPE_DETECT_THRESHOLD 1.3
#endif

/*-----------------------------------------------------------------------------
                                   Function prototypes
 -----------------------------------------------------------------------------*/

cpl_error_code visir_destripe_image(cpl_image *, int, double, double,
                                    cpl_boolean);

#endif
